#pragma once
#include "Requests.h"
#include "Buffer.h"

class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(Buffer buff);
	static SignupRequest deserializeSignupRequest(Buffer buff);
	static GetPlayersInRoomRequest deserializeGetPlayersInRoomRequest(Buffer buff);
	static JoinRoomRequest deserializeJoinRoomRequest(Buffer buff);
	static CreateRoomRequest deserializeCreateRoomRequest(Buffer buff);
};