#include "JsonRequestPacketDeserializer.h"

/*
Deserializes a Login request message.
input: message buffer.
output: LoginRequest struct.
*/

LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(Buffer buff)
{
    LoginRequest result;
    json jsonData = buff.toJson();
    result.username = jsonData["username"].get<std::string>();
    result.password = jsonData["password"].get<std::string>();
    return result;
}

/*
Deserializes a Signup request message.
input: message buffer.
output: SignupRequest struct.
*/
SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(Buffer buff)
{
    SignupRequest result;
    json jsonData = buff.toJson();
    result.username = jsonData["username"].get<std::string>();
    result.password = jsonData["password"].get<std::string>();
    result.email = jsonData["email"].get<std::string>();
    result.address = jsonData["address"].get<std::string>();
    result.phone = jsonData["phone"].get<std::string>();
    result.birthDate = jsonData["birthDate"].get<std::string>();
    return result;
}

/*
Deserializes a Get Players In Room request message.
input: message buffer.
output: GetPlayersInRoomRequest struct.
*/
GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(Buffer buff)
{
    GetPlayersInRoomRequest result;
    json jsonData = buff.toJson();
    result.roomId = jsonData["roomId"];
    return result;
}

/*
Deserializes a Join Room request message.
input: message buffer.
output: JoinRoomRequest struct.
*/
JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(Buffer buff)
{
    JoinRoomRequest result;
    json jsonData = buff.toJson();
    result.roomId = jsonData["roomId"];
    return result;
}

/*
Deserializes a Create Room request message.
input: message buffer.
output: CreateRoom struct.
*/
CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(Buffer buff)
{
    CreateRoomRequest result;
    json jsonData = buff.toJson();
    result.roomName = jsonData["roomName"].get<std::string>();
    result.questionCount = jsonData["questionCount"];
    result.answerTimeout = jsonData["answerTimeout"];
    result.maxUsers = jsonData["maxUsers"];
    return result;
}


