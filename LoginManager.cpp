#include "LoginManager.h"
#include <algorithm>
#include <regex>

LoginManager::LoginManager(IDatabase* db):m_database(db){ }

/*
checks if the user is exsist if not the 
func wiil create a new user
input: username, password, email
output: none
*/
bool LoginManager::signup(const std::string& username, const std::string& password, const std::string& email, const std::string& address, const std::string& phone, const std::string& birthDate)
{
	std::smatch matches;
	std::regex reg_password("^(?=.*[A-Z])(?=.*[a-z])(?=^\\D*\\d\\D*$)(?=^[^!@#$%^&*]*[!@#$%^&*][^!@#$%^&*]*$)(^.{8}$).*$");
	std::regex reg_email("(\\w + )(\\. | _) ? (\\w)@(\\w + )(\\.(\\w + )) + ");
	std::regex reg_address("^\\([aA-zZ]+,\\d+,[aA-zZ]+\\)$");
	std::regex reg_phone("^0\\d{1,2}-\\d{7}$");
	std::regex reg_birthdate(" ^ \\d\\d\\.\\d\\d\\.\\d\\d\\d\\d$ | ^ \\d\\d\\ / \\d\\d\\ / \\d\\d\\d\\d$");

	if (!this->m_database->doesUserExist(username)&& std::regex_match(password, reg_password) && std::regex_match(email, reg_email) && std::regex_match(phone, reg_phone) && std::regex_match(birthDate, reg_birthdate) && std::regex_match(address, reg_address))
	{
		this->m_database->addNewUser(username, password, email, address, phone, birthDate);
		return true;
	}
	else
	{
		return false;
	}
}

/*
checks if the user is logged in if no aff to the loggin list
input: username
output: none
*/
bool LoginManager::login(const std::string& username, const std::string& password)
{
	if (this->m_database->doesUserExist(username) &&
		this->m_database->doesPasswordMatch(username, password) &&
		std::find(m_LoggedUsers.begin(), m_LoggedUsers.end(), username) == m_LoggedUsers.end())
	{
		std::cout << "it work" << std::endl;
		this->m_LoggedUsers.push_back(username);
		return true;
	}
	else
	{
		std::cout << "it didnt work" << std::endl;
		return false;
	}
}

/*
checks if the user is exsist if the user 
exsist the func will disconnect him
input: user name
output: none
*/
bool LoginManager::logout(const std::string& username)
{
	auto it = std::find(this->m_LoggedUsers.begin(), this->m_LoggedUsers.end(), username);
	if (it != this->m_LoggedUsers.end())
	{
		this->m_LoggedUsers.erase(it);
		return true;
	}
	else
	{
		return false;
	}
}
