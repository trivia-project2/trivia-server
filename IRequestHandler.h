#pragma once
#include <ctime>
#include "Buffer.h"
#include "Requests.h"
#include "LoginManager.h"

struct RequestInfo;
struct RequestResult;
class RequestHandlerFactory;
class LoginRequestHandler;

class IRequestHandler
{
public:
	virtual bool isRequestRelevant(RequestInfo) = 0;
	virtual RequestResult handleRequest(RequestInfo) = 0;	
};
