#include "SqliteDataBase.h"

//c'tor
SqliteDataBase::SqliteDataBase()
{
	sqlite3* db;
	std::string dbFileName = "triviaDB.sqlite";
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &db);// open file
	this->m_db = db;

	if (doesFileExist)
	{
		std::cout << "Creating new DB named " << dbFileName << std::endl;
		this->newDB();
	}
}

/*
close the data base
input: none
output: none
*/
SqliteDataBase::~SqliteDataBase()
{
	sqlite3_close(this->m_db);
	this->m_db = nullptr;
}

/* search if the user exsist
* input: user to found
* output : if the user exsist or not
*/
bool SqliteDataBase::doesUserExist(const std::string& user_name) 
{
	
	std::string sqlUser = "SELECT * FROM USERS WHERE NAME='" + user_name + "';";
	std::list<std::string> listOfUsers;
	this->sqlite3_exe(sqlUser, usersCallback, &listOfUsers);
	return listOfUsers.size() != 0;
}

/* add new user to the data base
* input: user name, password and email
* output : none
*/
bool SqliteDataBase::doesPasswordMatch(const std::string& user_name, const std::string& password) 
{
	std::string sqlUser = "SELECT * FROM USERS WHERE NAME='" + user_name + "' AND PASSWORD='" + password +"';";
	std::list<std::string> listOfUsers;
	this->sqlite3_exe(sqlUser, usersCallback, &listOfUsers);
	return listOfUsers.size() != 0;
}

/* add new user to the data base
* input: user name, password and email
* output : none
*/
void SqliteDataBase::addNewUser(const std::string& user_name, const std::string& password, const std::string& email, const std::string& address, const std::string& phone, const std::string& birth_Date)
{
	std::string sqlSelement = "INSERT INTO USERS (NAME, PASSWORD, EMAIL, PASSWORD, PHONE , BIRTH_DATE ) VALUES ('" + user_name + "', '" + password + "','" + email + "','" + "'" + address + "', '" + phone + "', '" + birth_Date + ");";
	std::cout << sqlSelement << std::endl;
	sqlite3_exe(sqlSelement);
}

/*
* input: a string of username
* output: number of total answers he answered
* A method that gives the number of answers the username answered
*/
int SqliteDataBase::getNumOfTotalAnswers(std::string username)
{
	int res = 0, result = 0;
	char* errorMsg;
	const std::string sqlQuery = "SELECT TOTAL_ANSWERS FROM USERS WHERE NAME='" + username + "';";
	result = getFirstInt(sqlQuery);
	return result;
}

/*
* input: a string of username
* output: number of correct answer the username answered
*/
int SqliteDataBase::getNumOfCorrectAnswers(std::string username)
{
	int res = 0, result = 0;
	char* errorMsg;
	const std::string sqlQuery = "SELECT RIGHT_ANSWERS FROM USERS WHERE NAME='" + username + "';";
	result = getFirstInt(sqlQuery);
	return result;
}

/*
* input: a string of username
* output: an int of how many games he played
*/
int SqliteDataBase::getNumOfPlayers(std::string username)
{
	int res = 0, result = 0;
	char* errorMsg;
	const std::string sqlQuery = "SELECT NUM_OF_GAMES FROM USERS WHERE NAME='" + username + "';";
	result = getFirstInt(sqlQuery);
	return result;
}

/*
* input: a string of username
* output: a float of the number time of answer of a user (in ms)
* A function that calculates the average time beetwen the answers of a username
*/
float SqliteDataBase::getPlayerAverageTime(std::string username)
{
	char* errorMsg;
	int totalTime = 0;
	const std::string sqlQuery = "SELECT TOTAL_TIME FROM USERS WHERE NAME='" + username + "';";
	totalTime = getFirstInt(sqlQuery);
	return (float)totalTime / (float)this->getNumOfTotalAnswers(username);
}

std::vector<std::string> SqliteDataBase::getAllUsernames()
{
	const std::string sqlQuery = "SELECT name USERS;";
	std::vector<std::string> result;
	char* errorMsg;
	auto callback = [](void* data, int argc, char** argv, char** colName)
	{
		for (int i = 0; i < argc; i++)
		{
			(*((std::vector<std::string>*)data)).push_back(argv[i]);
		}
		
		return 0;
	};
	sqlite3_exec(this->m_db, sqlQuery.c_str(), callback, &result, &errorMsg);
	return result;
}

/* create new db
* input: none
* output : none
*/
void SqliteDataBase::newDB() 
{
	const std::string sqlCreateUser = "CREATE TABLE USERS(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT NOT NULL, EMAIL TEXT NOT NULL, PASSWORD TEXT NOT NULL,  ADDRESS TEXT NOT NULL, PHONE TEXT NOT NULL, BIRTH_DATE TEXT NOT NULL,TOTAL_ANSWERS INTEGER DEFAULT 0, RIGHT_ANSWERS INTEGER DEFAULT 0, NUM_OF_GAMES INTEGER DEFAULT 0, TOTAL_TIME INTEGER DEFAULT 0);";
	this->sqlite3_exe(sqlCreateUser);
	const std::string sqlCreateQuestions = "CREATE TABLE QUESTIONS(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, CONTENT TEXT NOT NULL, AMERICAN INTEGER DEFAULT 0);";
	this->sqlite3_exe(sqlCreateQuestions);
	const std::string sqlCreateAnswers = "CREATE TABLE ANSWERS(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, IS_TRUE INTEGER DEFAULT 0, QUESTION_ID INTEGER, CONTENT TEXT NOT NULL, FOREIGN KEY(QUESTION_ID) REFERENCES QUESTIONS(ID));";
	this->sqlite3_exe(sqlCreateAnswers);
}

int SqliteDataBase::getFirstInt(std::string sqlStatement)
{
	int res = 0, result = 0;
	char* errorMsg;
	auto callback = [](void* data, int argc, char** argv, char** colName)
	{
		*((int*)data) = std::atoi(argv[0]);
		return 0;
	};
	res += sqlite3_exec(this->m_db, sqlStatement.c_str(), callback, &result, &errorMsg);
	if (res != SQLITE_OK)
	{
		std::cout << errorMsg << std::endl;
		return -1;
	}
	return result;
}

/*do the selementand check if
* the is ligal
* input: sql Selement
* output : none
*/
void SqliteDataBase::sqlite3_exe(std::string sqlSelement)
{
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->m_db, sqlSelement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << std::string(errMessage) << std::endl;
		//throw MyException(std::string(errMessage));
	}

}

/*
* do the selement and check if
* the is ligal
* input: sql Selement, call back func, refernce to data
*/
void SqliteDataBase::sqlite3_exe(std::string sqlSelement, int(*callback)(void*, int, char**, char**), void* callbackParam)
{
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->m_db, sqlSelement.c_str(), callback, callbackParam, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << errMessage << std::endl;
		//throw MyException(std::string(errMessage));
	}
}


/*
callback function to receive data from the sqlite3 library. runs for every row of results in a query.
input: pointer to data object, amount of columns in result, values of the columns, names of the columns.
output: 0.
*/
int usersCallback(void* data, int argc, char** argv, char** azColName)
{
	std::list<std::string>* now = (std::list<std::string>*)data;
	std::string currUser = "";
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "NAME")
			currUser = argv[i];
	}
	now->push_back(currUser);
	return 0;
}
