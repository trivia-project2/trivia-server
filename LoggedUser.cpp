#include "LoggedUser.h"

//c'tor
LoggedUser::LoggedUser(std::string user_name)
{
    this->m_username = user_name;
}

/*
* return user name
* input: none
* output: none
*/
std::string LoggedUser::getUsername() const
{
    return this->m_username;
}


bool LoggedUser::operator==(const std::string& other) const
{
    return this->m_username == other;
}

bool LoggedUser::operator==(const LoggedUser& other) const
{
    return this->getUsername() == other.getUsername();
}
