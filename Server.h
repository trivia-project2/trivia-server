#pragma once
#include <map>
#include <WinSock2.h>
#include <Windows.h>

#include "Communicator.h"
#include "LoginRequestHandler.h"


class Server
{
public:
	Server() {}; //CTOR
	~Server() {}; //DTOR

	void run(int port);

private:
	Communicator m_Communicator;

};
