import socket
import json
import time

EXIT = 3

def main():
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	try:
		sock.connect(("127.0.0.1", 8876))
		user_input = 0
		
		while user_input != EXIT:
			print_menu()
			user_input = int(input("What is your choice? "))
			
			if user_input == 1:
				login(sock)
			elif user_input == 2:
				signup(sock)
				
	except Exception as e:
		print(str(e))

def login(sock):
	username = input("What is your username? ")
	password = input("What is your password? ")
	
	login_dict = {"username": username, "password": password}
	json_string = json.dumps(login_dict)
	sock.send(bytes([1]))
	sock.send(bytes(var_to_bytes(len(json_string))))
	sock.send(json_string.encode())
	print(sock.recv(1024)[5:].decode())

def signup(sock):
	username = input("What is your username? ")
	password = input("What is your password? ")
	email = input("What is your email? " )
	
	signup_dict = {"username": username, "password": password, "email": email}
	json_string = json.dumps(signup_dict)
	sock.send(bytes([2]))
	sock.send(bytes(var_to_bytes(len(json_string))))
	sock.send(json_string.encode())
	print(sock.recv(1024)[5:].decode())
		
def print_menu():
	print("""What would you like to do?
1. Login
2. Signup
3. Exit""")


def var_to_bytes(number):
	array = [0, 0, 0, 0]
	array[3] = (number>>24) & 0xff
	array[2] = (number>>16) & 0xff
	array[1] = (number>>8) & 0xff
	array[0] = number & 0xff
	
	return array

if __name__ == "__main__":
	main()
