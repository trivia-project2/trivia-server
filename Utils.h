#pragma once
#include <Windows.h>
#include <string>
#include "Buffer.h"

#define BYTES_IN_INT 4

class Utils
{
public:
	/*
	* input: a socket
	* output: the variable in the type given as template
	* A static function that gets from the socket the asked type (if its a number)
	*/
	template<class Type>
	static Type getIntPartFromSocket(SOCKET sc)
	{
		char* s = getPartFromSocket(sc, sizeof(Type), 0);
		Type num = *(Type*)s;
		delete[] s;
		return num;
	};

	static std::string getStringPartFromSocket(SOCKET sc, int bytesNum);
	static void sendData(SOCKET sc, Buffer buff);

	static char* getPartFromSocket(SOCKET sc, int bytesNum);

private:
	static char* getPartFromSocket(SOCKET sc, int bytesNum, int flags);
};