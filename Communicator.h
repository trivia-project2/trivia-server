#pragma once

#include <Windows.h>
#include <map>
#include <iostream>
#include <thread>
#include "IRequestHandler.h"
#include "LoginRequestHandler.h"
#include "Utils.h"
#include "RequestHandlerFactory.h"

class Communicator
{
public:
	Communicator(); //CTOR
	~Communicator(); //DTOR
	void startHandleRequests(int port); //Start handleing requests

	RequestHandlerFactory& getRequestHandler();

private:
	SOCKET m_serverSocket; //server socket
	std::map<SOCKET, IRequestHandler*> _users; //the map of the clients

	RequestHandlerFactory m_requestHandler;
	void bindAndListen(int port); //bind to the port
	void acceptClient(); //accept new client
	void handleNewClient(SOCKET newClient); //new client handler

};