#include "Communicator.h"
#include "json.hpp"

/*
* CTOR
*/
Communicator::Communicator()
{
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	this->m_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
}

/*
* DTOR
*/
Communicator::~Communicator()
{
	for (auto it = this->_users.begin(); it != this->_users.end(); ++it)
	{
		delete it->second;
	}

	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(this->m_serverSocket);
	}
	catch (...) {}
}

/*
* input: a port
* output: none
* A method that binds to the port given and accepts clients over and over again
*/
void Communicator::startHandleRequests(int port)
{
	bindAndListen(port);

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		acceptClient();
	}
}

RequestHandlerFactory& Communicator::getRequestHandler()
{
	return this->m_requestHandler;
}

/*
* input: a port
* output: none
* A method that binds on a port
*/
void Communicator::bindAndListen(int port)
{

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(m_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(m_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;


}

/*
* input: none
* output: none
* A method that accepts a new client, adds it to the map, and starts a thread
*/
void Communicator::acceptClient()
{
	SOCKET client_socket = ::accept(m_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// the function that handle the conversation with the client
	this->_users.insert(std::pair<SOCKET, IRequestHandler*>(client_socket, this->m_requestHandler.createLoginRequestHandler()));
	std::thread clientThread(&Communicator::handleNewClient, this, client_socket);
	clientThread.detach();
}

/*
* input: the socket
* output: none
* A method that is responsble for the new client (in the new thread)
*/
void Communicator::handleNewClient(SOCKET newClient)
{
	int msgLength = 0;
	auto client = this->_users.find(newClient);

	while (this->_users[newClient] != nullptr)
	{
		Buffer buff;
		try
		{
			buff.addByte(Utils::getIntPartFromSocket<byte>(newClient));
			msgLength = Utils::getIntPartFromSocket<unsigned int>(newClient);
			buff.addInt(msgLength);
			buff.addString(Utils::getStringPartFromSocket(newClient, msgLength));
		}
		catch (const std::exception& e)
		{
			std::cout << "A client disconnected" << std::endl;
			closesocket(newClient);

			//deleting the client from the map and freeing memory
			delete client->second;
			this->_users.erase(client);
			return;
		}

		//DEBUG PRINTING
		std::cout << "OPCODE: " << (char)('0' + buff[0]) << " LEGNTH: " << msgLength << std::endl;
		std::cout << buff.toJson().dump() << std::endl;

		RequestInfo requestInfo;
		requestInfo.buffer = buff;
		requestInfo.id = buff[0];
		time(&requestInfo.recevialTime);
		

		if (client->second->isRequestRelevant(requestInfo))
		{
			RequestResult result = client->second->handleRequest(requestInfo);
			
			//sending msg
			Utils::sendData(newClient, result.response);

			if (result.newHandler != nullptr)
			{
				delete this->_users[newClient];
				this->_users[newClient] = result.newHandler;
			}
		}
		else
		{
			ErrorResponse errorResponse;
			errorResponse.message = "Your msg id is not vallid!";
			
			Buffer answer = JsonResponsePacketSerializer::serializeResponse(errorResponse);
			Utils::sendData(newClient, answer);
		}
	} 
}
