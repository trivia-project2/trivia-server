#pragma once
#include "IDataBase.h"
#include "LoginManager.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "StatisticsManager.h"
#include "RoomManager.h"

class LoginRequestHandler;
class LoginManager;
class MenuRequestHandler;

class RequestHandlerFactory
{
public:
	RequestHandlerFactory();
	~RequestHandlerFactory();

	LoginManager& getLoginManager();
	LoginRequestHandler* createLoginRequestHandler();
	StatisticsManager& getStatisticsManager();
	MenuRequestHandler* createMenuRequestHandler();
	RoomManager& getRoomManager();

private:
	IDatabase* m_database;
	LoginManager* m_loginManager;
	StatisticsManager* m_statisticsManager;
	RoomManager* m_roomManager;
};