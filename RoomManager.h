#pragma once
#include "Room.h"
#include <string>
#include <map>
#include <vector>

class RoomManager
{
public:
	void createRoom(LoggedUser, RoomData); // create a room
	void joinRoom(LoggedUser, int roomId);
	void deleteRoom(int roomId); // delete a room
	unsigned int getRoomState(int roomId); //room state
	std::map<int, Room> getRooms(); //get all rooms
	std::vector<RoomData> getRoomData();

private:
	std::map<int, Room> m_rooms;
};

