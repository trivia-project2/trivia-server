#pragma once
#include <vector>
#include <string>
#include "json.hpp"

#define JSON_START 5
#define LEN_START 2
#define BYTES_IN_INT 4

/* unsigned char is a fully positive char.
* we know that char is 1 byte long, so that
* unsigned char can represent a byte */
typedef unsigned char byte;

using json = nlohmann::json;

class Buffer : public std::vector<byte>
{
public:
	json toJson(); // get as nlohman::json object
	byte getOpCode(); // get first byte of buffer (OPCODE)
	int getJsonLength(); // get the length of the json part

	void addString(std::string toAdd); // add a string to the buffer
	void addInt(int num); // add an int to the buffer(spreading it on 4 bytes)
	void addByte(byte num); // inserts a byte to the buffer


	int getLength(); //returns the number of bytes in the buffer
	bool isEmpty(); // if the buffer is empty
};