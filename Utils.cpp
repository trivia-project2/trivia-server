#include "Utils.h"


std::string Utils::getStringPartFromSocket(SOCKET sc, int bytesNum)
{
	char* s = getPartFromSocket(sc, bytesNum, 0);
	std::string res(s);
	return res;
}

void Utils::sendData(SOCKET sc, Buffer buff)
{
	byte* bytesArr = buff.data();
	send(sc, (char*)bytesArr, buff.getLength(), 0);
}


// recieve data from socket according byteSize
// this is private function
char* Utils::getPartFromSocket(SOCKET sc, int bytesNum)
{
	return getPartFromSocket(sc, bytesNum, 0);
}

char* Utils::getPartFromSocket(SOCKET sc, int bytesNum, int flags)
{
	if (bytesNum == 0)
	{
		return (char*)"";
	}

	char* data = new char[bytesNum + 1];

	int size_to_recv = bytesNum;
	int r;
	while (size_to_recv)
	{
		r = recv(sc, data, size_to_recv, 0);

		if (r <= 0)
			break; //socket closed or an error occurred
		size_to_recv -= r;
	}

	if (r == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(sc);
		throw std::exception(s.c_str());
	}

	data[bytesNum] = 0;
	return data;
}

