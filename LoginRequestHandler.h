#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"

class RequestHandlerFactory;

class LoginRequestHandler : public IRequestHandler
{
public:
	LoginRequestHandler(LoginManager* m_loginManager, RequestHandlerFactory* m_HandlerFactory);
	
	virtual bool isRequestRelevant(RequestInfo);
	virtual RequestResult handleRequest(RequestInfo);

private:
	RequestResult login(RequestInfo request);
	RequestResult signup(RequestInfo request);

	LoginManager* m_loginManager;
	RequestHandlerFactory* m_HandlerFactory;
};