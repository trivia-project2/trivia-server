#pragma once
#include <string>
#include <vector>

class IDatabase
{
public:
	IDatabase() = default;
	~IDatabase() = default;
	virtual bool doesUserExist(const std::string& user_name) = 0;
	virtual bool doesPasswordMatch(const std::string& user_name, const std::string& password)  = 0;
	virtual void addNewUser(const std::string& username, const std::string& password, const std::string& email, const std::string& address, const std::string& phone, const std::string& birthDate) = 0;
	
	virtual int getNumOfTotalAnswers(std::string username) = 0;
	virtual int getNumOfCorrectAnswers(std::string username) = 0;
	virtual int getNumOfPlayers(std::string username) = 0;
	virtual float getPlayerAverageTime(std::string username) = 0;
	virtual std::vector<std::string> getAllUsernames() = 0;
};
