#include "RequestHandlerFactory.h"

RequestHandlerFactory::RequestHandlerFactory()
{
	this->m_database = new SqliteDataBase();
	this->m_loginManager = new LoginManager(m_database);
	this->m_statisticsManager = new StatisticsManager(this->m_database);
}

RequestHandlerFactory::~RequestHandlerFactory()
{
	delete m_database;
	delete m_loginManager;
	delete m_statisticsManager;
}

LoginManager& RequestHandlerFactory::getLoginManager()
{
	return *m_loginManager;
}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	return new LoginRequestHandler(m_loginManager, this);
}

StatisticsManager& RequestHandlerFactory::getStatisticsManager()
{
	return *m_statisticsManager;
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler()
{
	return new MenuRequestHandler(this->m_statisticsManager, this->m_roomManager , this);
}


RoomManager& RequestHandlerFactory::getRoomManager()
{
	return *m_roomManager;
}
