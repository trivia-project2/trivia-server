import socket

def main():
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	try:
		sock.connect(("127.0.0.1", 8876))
		sock.send(b"Hello")
		print(sock.recv(1024).decode())
	except Exception as e:
		print(str(e))

if __name__ == "__main__":
	main()
