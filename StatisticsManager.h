#pragma once
#include <vector>
#include <string>
#include <unordered_map>
#include <algorithm>
#include "IDataBase.h"

class StatisticsManager
{
public:
	StatisticsManager(IDatabase* m_database);
	std::vector<std::string> getHighScore();
	std::vector<std::string> getUserStatistics(const std::string& username);
	bool comp(std::pair<std::string, float> a, std::pair<std::string, float> b);
private:
	IDatabase* m_database;
};