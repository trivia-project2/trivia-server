#include "Server.h"
#include <exception>
#include <iostream>
#include <string>

/*
* input: a port
* output: none
* A function that starts the server on the port given.
*/
void Server::run(int port)
{
	std::cout << "Server startup on port " << port << std::endl;
	this->m_Communicator.startHandleRequests(port);
}

