#pragma once
#include "SqliteDataBase.h"
#include "LoggedUser.h"

class LoginManager
{
public:
	LoginManager(IDatabase* db);
	~LoginManager() {};
	bool signup(const std::string& username, const std::string& password, const std::string& email, const std::string& address, const std::string& phone, const std::string& birthDate);
	bool login(const std::string& username, const std::string& password);
	bool logout(const std::string& username);

private:
	IDatabase* m_database;
	std::vector<LoggedUser> m_LoggedUsers;
};


