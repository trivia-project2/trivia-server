#include "MenuRequestHandler.h"

MenuRequestHandler::MenuRequestHandler(StatisticsManager* statisticsManager, RoomManager* roomManager, RequestHandlerFactory* handlerFactory):IRequestHandler()
{
	this->m_handlerFactory = handlerFactory;
	this->m_roomManager = roomManager;
	this->m_statisticsManager = statisticsManager;
    this->m_user = nullptr;
}

bool MenuRequestHandler::isRequestRelevant(RequestInfo request)
{
    switch (request.id)
    {
    case OP_CODES::CREAT_ROOM:
        return true;
    case OP_CODES::GET_ROOMS:
        return true;
    case OP_CODES::HIGH_SCORE:
        return true;
    case OP_CODES::JOIN_ROOM:
        return true;
    case OP_CODES::LOGOUT:
        return true;
    case OP_CODES::PLAYERS_IN_ROOM:
        return true;
    default:
        return false;
    }
}

RequestResult MenuRequestHandler::handleRequest(RequestInfo request)
{
    byte opcode = request.buffer.getOpCode();

    switch (request.id)
    {
    case OP_CODES::CREAT_ROOM:
        return this->createRoom(request);
    case OP_CODES::GET_ROOMS:
        return this->getRooms(request);
    case OP_CODES::HIGH_SCORE:
        return this->getStatistics(request);
    case OP_CODES::JOIN_ROOM:
        return this->joinRoom(request);
    case OP_CODES::LOGOUT:
        return this->logout(request);
    case OP_CODES::PLAYERS_IN_ROOM:
        return this->getPlayersInRoom(request);
    }

    RequestResult result;
    ErrorResponse errorResponse;
    errorResponse.message = "ERROR: Unkown opcode!"; // TO-DO: ENUM of errors
    result.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
    result.newHandler = nullptr;
    return result;
}

RequestResult MenuRequestHandler::logout(RequestInfo info)
{
    RequestResult result;
    LogoutResponse logoutResponse;
    logoutResponse.status = (unsigned int)true;
    result.response = JsonResponsePacketSerializer::serializeResponse(logoutResponse);
    result.newHandler = this->m_handlerFactory->createLoginRequestHandler();
    return result;
}

RequestResult MenuRequestHandler::getRooms(RequestInfo info)
{
    RequestResult result;
    GetRoomsResponse getRoomsResponse;
    getRoomsResponse.rooms = this->m_roomManager->getRoomData();
    getRoomsResponse.status = true;
    result.newHandler = nullptr;
    return result;
}

RequestResult MenuRequestHandler::getStatistics(RequestInfo info)
{
    RequestResult result;
    HighScoreResponse highScoreResponse;
    highScoreResponse.statistics = this->m_statisticsManager->getHighScore();
    highScoreResponse.status = true;
    result.newHandler = nullptr;
    return result;
}

RequestResult MenuRequestHandler::joinRoom(RequestInfo info)
{
    RequestResult result;
    JoinRoomResponse joinRoomResponse;
    JoinRoomRequest joinRoomRequest = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(info.buffer);
    
    if (joinRoomRequest.roomId < this->m_roomManager->getRooms().size())
    {
        this->m_roomManager->joinRoom(*this->m_user, joinRoomRequest.roomId);
        joinRoomResponse.status = (int)true;
    }
    else
    {
        joinRoomResponse.status = (int)false;
    }
    result.newHandler = nullptr;
    result.response = JsonResponsePacketSerializer::serializeResponse(joinRoomResponse);
    return result;
}

RequestResult MenuRequestHandler::createRoom(RequestInfo info)
{
    RequestResult result;
    CreateRoomResponse  createRoomResponse; 
    CreateRoomRequest  createRoomRequest = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(info.buffer);
    RoomData room;

    room.maxPlayers = createRoomRequest.maxUsers;
    room.numOfQuestionslnGame = createRoomRequest.questionCount;
    room.id = this->m_roomManager->getRooms().size();
    room.currPlayers = 1;
    room.isActive = true;
    room.timePerQuestion = createRoomRequest.answerTimeout;
    room.name = createRoomRequest.roomName;

    this->m_roomManager->createRoom(*this->m_user, room);
    this->m_user;

    result.newHandler = nullptr;
    createRoomResponse.status = true;
    result.response = JsonResponsePacketSerializer::serializeResponse(createRoomResponse);
    return result;
}

RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo info)
{
    RequestResult result;
    GetPlayersInRoomResponse getPlayersInRoomResponse;
    GetPlayersInRoomRequest  getPlayersInRoomRequest = JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(info.buffer);
    std::map<int, Room> rooms = this->m_roomManager->getRooms();
    getPlayersInRoomResponse.players = rooms[getPlayersInRoomRequest.roomId].getAllUsers();
    result.newHandler = nullptr;
    result.response = JsonResponsePacketSerializer::serializeResponse(getPlayersInRoomResponse);
    return result;
}
