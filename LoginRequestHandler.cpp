#include "LoginRequestHandler.h"

LoginRequestHandler::LoginRequestHandler(LoginManager* m_loginManager, RequestHandlerFactory* m_HandlerFactory)
    :IRequestHandler()
{
    this->m_loginManager = m_loginManager;
    this->m_HandlerFactory = m_HandlerFactory;
}

bool LoginRequestHandler::isRequestRelevant(RequestInfo request)
{
    switch (request.id)
    {
    case OP_CODES::LOGIN:
        return true;
    case OP_CODES::SIGNUP:
        return true;
    default:
        return false;
    }
}

RequestResult LoginRequestHandler::handleRequest(RequestInfo request)
{
    byte opcode = request.buffer.getOpCode();

    switch (opcode)
    {
    case OP_CODES::LOGIN:
        return this->login(request);
    case OP_CODES::SIGNUP:
        return this->signup(request);
    }

    RequestResult result;
    ErrorResponse errorResponse;
    errorResponse.message = "ERROR: Unkown opcode!"; // TO-DO: ENUM of errors
    result.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
    result.newHandler = nullptr;
    return result;
}

RequestResult LoginRequestHandler::login(RequestInfo request)
{
    RequestResult result;

    LoginResponse loginResponse;
    LoginRequest loginRequest = JsonRequestPacketDeserializer::deserializeLoginRequest(request.buffer);
    
    bool logged = this->m_loginManager->login(loginRequest.username, loginRequest.password);
    loginResponse.status = (int)logged;
    result.response = JsonResponsePacketSerializer::serializeResponse(loginResponse);
    result.newHandler = nullptr;
    return result;
}

RequestResult LoginRequestHandler::signup(RequestInfo request)
{
    RequestResult result;
    SignupResponse signupResponse;
    SignupRequest signupRequest = JsonRequestPacketDeserializer::deserializeSignupRequest(request.buffer);
    
    bool isSigned = this->m_loginManager->signup(signupRequest.username, signupRequest.password, signupRequest.email, signupRequest.address, signupRequest.phone, signupRequest.birthDate);
    signupResponse.status = (int)isSigned;

    result.newHandler = nullptr;
    result.response = JsonResponsePacketSerializer::serializeResponse(signupResponse);
    return result;
}
