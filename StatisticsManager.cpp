#include "StatisticsManager.h"

StatisticsManager::StatisticsManager(IDatabase* m_database)
{
    this->m_database = m_database;
}

std::vector<std::string> StatisticsManager::getHighScore()
{
    std::vector<std::string> allUsernames = this->m_database->getAllUsernames();
    std::unordered_map<std::string, float> theScores;

    for (int i = 0; i < allUsernames.size(); i++)
    {
        float averageTime = this->m_database->getPlayerAverageTime(allUsernames[i]);
        float correctAnswers = this->m_database->getNumOfCorrectAnswers(allUsernames[i]);
        float finalScore = correctAnswers / averageTime;
        theScores.insert(std::pair<std::string, float>(allUsernames[i], finalScore));
    }
    std::vector<std::pair<std::string, float>> elems(theScores.begin(), theScores.end());
    //std::sort(elems.begin(), elems.end(), comp);// this line need to be fix 

    std::vector<std::string> result;
    for (int i = 0; i < elems.size(); i++)
    {
        result.push_back(elems[i].first);
    }
    return result;
}

std::vector<std::string> StatisticsManager::getUserStatistics(const std::string& username)
{
    float averageTime = this->m_database->getPlayerAverageTime(username);
    float correctAnswers = this->m_database->getNumOfCorrectAnswers(username);
    float finalScore = correctAnswers / averageTime;
    std::vector<std::string> result;
    result.push_back("Average time of answers: " + std::to_string(averageTime));
    result.push_back("Correct Answers: " + std::to_string(correctAnswers));
    result.push_back("Final score: " + std::to_string(finalScore));
    return result;
}

bool StatisticsManager::comp(std::pair<std::string, float> a, std::pair<std::string, float> b)
{
    return a.second < b.second;
}

