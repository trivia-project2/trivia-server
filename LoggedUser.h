#pragma once
#include <string>

class LoggedUser
{
public:
	LoggedUser(std::string user_name);//c'tor
	~LoggedUser() {};//d'tor
	std::string getUsername() const;//get user name

	bool operator==(const std::string& other) const;
	bool operator==(const LoggedUser& other) const;

private:
	std::string m_username;
};

