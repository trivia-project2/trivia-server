#pragma once
#include "IDatabase.h"
#include "sqlite3.h"
#include <io.h>
#include <vector>
#include <iostream>
#include <list>
#include <list>

int usersCallback(void* data, int argc, char** argv, char** azColName);

class SqliteDataBase: public IDatabase
{
public:
	SqliteDataBase();//c'tor
	~SqliteDataBase();//d'tor
	bool doesUserExist(const std::string& user_name)  override;
	bool doesPasswordMatch(const std::string& user_name, const std::string& password)  override;
	void addNewUser(const std::string& username, const std::string& password, const std::string& email, const std::string& address, const std::string& phone, const std::string& birthDate) override;
  

	virtual int getNumOfTotalAnswers(std::string username) override;
	virtual int getNumOfCorrectAnswers(std::string username) override;
	virtual int getNumOfPlayers(std::string username) override;
	virtual float getPlayerAverageTime(std::string username) override;
	virtual std::vector<std::string> getAllUsernames() override;

private:
	sqlite3* m_db;
	void newDB();
	int getFirstInt(std::string sqlStatement);
	void sqlite3_exe(std::string sqlSelement);
	void sqlite3_exe(std::string sqlSelement, int(*callback)(void*, int, char**, char**), void* callbackParam);
};

