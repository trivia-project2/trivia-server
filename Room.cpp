#include "Room.h"

//Deafult CTOR
Room::Room()
{
}

//CTOR
Room::Room(RoomData data)
{
	this->m_metadata = data;
}

/*
* input: a user
* output: none
* A method that adds a user into a game room
*/
void Room::addUser(const LoggedUser& user)
{
	m_users.push_back(user);
}

/*
* input: a user
* output: none
* A method that removes a user from the vector of users
*/
void Room::remove(const LoggedUser& user)
{
	m_users.erase(std::remove(m_users.begin(), m_users.end(), user), m_users.end());
}

/*
* input: none
* output: vector of all the username in the room
* A method that gives you all of the users in the room
*/
std::vector<std::string> Room::getAllUsers() const
{
	std::vector<std::string> allUsers;

	for (int i = 0; i < m_users.size(); i++)
	{
		allUsers.push_back(m_users[i].getUsername());
	}

	return allUsers;
}

/*
* RoomData getter
*/
RoomData Room::getRoomData() const
{
	return m_metadata;
}

bool Room::operator==(const Room& other) const
{
	return other.getRoomData().id == this->getRoomData().id;
}
