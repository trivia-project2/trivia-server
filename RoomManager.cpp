#include "RoomManager.h"

/*
* input: a user and a room data
* output: none
* A method that creates a game room so usesrs can log into
*/
void RoomManager::createRoom(LoggedUser user, RoomData data)
{
    this->m_rooms.insert(std::pair<int, Room>(data.id, Room(data)));
    m_rooms[m_rooms.size() - 1].addUser(user);
}

void RoomManager::joinRoom(LoggedUser user, int roomId)
{
    this->m_rooms[roomId].addUser(user);
}

/*
* input: room id
* output: none
* A method that deletes a room by his id
*/
void RoomManager::deleteRoom(int roomId)
{
    this->m_rooms.erase(roomId);
}

/*
* input: room id
* output: none
* The room state (0 if not active, 1 if its active)
*/
unsigned int RoomManager::getRoomState(int roomId)
{
    bool state = m_rooms[roomId].getRoomData().isActive;
    return (unsigned int)state;
}

std::map<int, Room> RoomManager::getRooms()
{
   return this->m_rooms;
}

/*
* input: none
* output: vector of rooms
* A method that gives you a vector of all the game rooms in the game.
*/
std::vector<RoomData> RoomManager::getRoomData()
{
    std::vector<RoomData> rooms;
    for (auto it = m_rooms.begin(); it != m_rooms.end(); ++it)
    {
        rooms.push_back(it->second.getRoomData());
    }
    return rooms;
}
