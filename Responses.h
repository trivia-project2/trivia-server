#pragma once
#include "Buffer.h"
#include "json.hpp"
#include <vector>


enum OP_CODES : byte
{
	ERORR,
	LOGIN,
	SIGNUP,
	LOGOUT,
	GET_ROOMS,
	PLAYERS_IN_ROOM,
	JOIN_ROOM, 
	CREAT_ROOM, 
	HIGH_SCORE
};

struct LoginResponse
{
	unsigned int status;
};

struct SignupResponse
{
	unsigned int status;
};

struct ErrorResponse
{
	std::string message;
};

struct LogoutResponse
{
	unsigned int status;
};

struct RoomData
{
	unsigned int id;
	std::string name;
	unsigned int maxPlayers;
	unsigned int currPlayers;
	unsigned int numOfQuestionslnGame;
	unsigned int timePerQuestion;
	bool isActive;
};

struct GetRoomsResponse
{
	unsigned int status;
	std::vector<RoomData> rooms;
};

struct GetPlayersInRoomResponse
{
	std::vector<std::string> players;
};

struct JoinRoomResponse
{
	unsigned int status;
};

struct CreateRoomResponse
{
	unsigned int status;
};

struct HighScoreResponse
{
	unsigned int status;
	std::vector<std::string> statistics;
};

struct GetPersonalStatsHighScoreResponse
{
	unsigned int status;
	std::vector<std::string> statistics;
};

struct CloseRoomResponse
{
	unsigned int status;
};

struct StartGameResponse
{
	unsigned int status;
};

struct GetRoomStateResponse
{
	unsigned int status;
	RoomData room;
	std::vector<std::string> players;
};

