#include "Buffer.h"

/*
* input: none
* output: a json object
* A method that takes the string of json from the buffer
* parses it to a string object, and then parses it to a json object to return it
*/
json Buffer::toJson()
{
    return json::parse(std::string(this->begin() + JSON_START, this->end()));
}

/*
* int: none
* output: a byte
* A function that gives the OpCode byte and returns it
*/
byte Buffer::getOpCode()
{
    return (byte)(*this)[0];
}

/*
* input: none
* output: the json msg length
* A method that returns the length from the buffer
* it parses the 4 bytes from the buffer and returns as int
*/
int Buffer::getJsonLength()
{
    byte intAsBytes[BYTES_IN_INT] = { 0 };
    for (int i = 0; i < BYTES_IN_INT; i++)
    {
        intAsBytes[i] = (*this)[LEN_START + i - 1];
    }
    int result = 0;
    memcpy(&result, intAsBytes, BYTES_IN_INT);
    return result;
}

/*
* input: a string to add to the buffer
* output: none
* A method that inserts a string of characters into the buffer
* it inserts it byte by byte
*/
void Buffer::addString(std::string toAdd)
{
    for (int i = 0; i < toAdd.size(); i++)
    {
        this->push_back((byte)toAdd[i]);
    }
}

/*
* input: int number
* output: none
* A method that parses the variable and turns it into 4 bytes
* and puts it into the buffer
*/
void Buffer::addInt(int num)
{
    byte intAsBytes[BYTES_IN_INT] = { 0 };
    memcpy(intAsBytes, &num, BYTES_IN_INT);

    for (int i = 0; i < BYTES_IN_INT; i++)
    {
        this->addByte(intAsBytes[i]);
    }
}

/*
* input: a byte
* output: none
* A method that adds a bytes to the buffer
*/
void Buffer::addByte(byte num)
{
    this->push_back(num);
}

/*
* input: none
* output: length
* A method that returns the length of the entire buffer
* (returns the amount of bytes it has)
*/
int Buffer::getLength()
{
    return this->size();
}

/*
* input: none
* output: a boolean 
* checks if the buffer is empty
*/
bool Buffer::isEmpty()
{
    return this->size() == 0;
}
