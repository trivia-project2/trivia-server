#pragma once
#include "LoggedUser.h"
#include "Responses.h"
#include <string>
#include <vector>
#include <algorithm>

class Room
{
public:
	Room();
	Room(RoomData data);

	void addUser(const LoggedUser& user);
	void remove(const LoggedUser& user);
	std::vector<std::string> getAllUsers() const;
	RoomData getRoomData() const;

	bool operator==(const Room& other) const;

private:
	RoomData m_metadata;
	std::vector<LoggedUser> m_users;

};

