#include "JsonResponsePacketSerializer.h"


/*
Serializes a generic protocol packet
input: json with datat , and the packet code.
output: buffer(byte array)
*/
Buffer JsonResponsePacketSerializer::response(json data, OP_CODES code)
{
    Buffer buff;
    buff.addByte((byte)code);
    std::string dataStr = data.dump();
    buff.addInt((int)dataStr.size());
    buff.addString(dataStr);
    return buff;
}
/*
Serializes a Login packet.
input: LoginResponse packet struct.
output: buffer - byte array.
*/

Buffer JsonResponsePacketSerializer::serializeResponse(LoginResponse res)
{
    json data;
    data["status"] = res.status;
    return response(data, OP_CODES::LOGIN);
}

/*
Serializes a singup packet.
input: SignupResponse packet struct.
output: buffer - byte array.
*/
Buffer JsonResponsePacketSerializer::serializeResponse(SignupResponse res)
{
    json data;
    data["status"] = res.status;
    return response(data, OP_CODES::SIGNUP);
}

/*
Serializes a Error packet.
input: ErrorResponse packet struct.
output: buffer - byte array.
*/
Buffer JsonResponsePacketSerializer::serializeResponse(ErrorResponse res)
{
    json data;
    data["message"] = res.message;
    return response(data, OP_CODES::ERORR);
}

/*
Serializes a Logout packet.
input: LogoutResponse packet struct.
output: buffer - byte array.
*/
Buffer JsonResponsePacketSerializer::serializeResponse(LogoutResponse res)
{
    json data;
    data["status"] = res.status;
    return response(data, OP_CODES::LOGOUT);
}

/*
Serializes a Get Rooms packet.
input: GetRoomsResponse packet struct.
output: buffer - byte array.
*/
Buffer JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse res)
{
    json data;
    data["status"] = res.status;

    std::string msg = "";
    for (auto& room : res.rooms)
    {
        msg += "\"name\": \"" + room.name + "\", ";
    }

    data["Rooms"] = msg;
    return response(data, OP_CODES::GET_ROOMS);
}

/*
Serializes a Get Players In Room packet.
input: GetPlayersInRoomResponse packet struct.
output: buffer - byte array.
*/
Buffer JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse res)
{
    json data;
    std::string msg = "";

    for (auto& player : res.players)
    {
        msg += player + ", ";
    }

    data["players"] = msg;
    return response(data, OP_CODES::PLAYERS_IN_ROOM);
}

/*
Serializes a Join Room packet.
input: JoinRoomResponse packet struct.
output: buffer - byte array.
*/
Buffer JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse res)
{
    json data;
    data["status"] = res.status;
    return response(data, OP_CODES::JOIN_ROOM);
}

/*
Serializes a Create Room packet.
input: CreateRoomResponse packet struct.
output: buffer - byte array.
*/
Buffer JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse res)
{
    json data;
    data["status"] = res.status;
    return response(data, OP_CODES::CREAT_ROOM);
}

/*
Serializes a High Score Response packet.
input: HighScoreResponse packet struct.
output: buffer - byte array.
*/
Buffer JsonResponsePacketSerializer::serializeResponse(HighScoreResponse res)
{
    json data;
    data["status"] = res.status;
    std::string msg = "";
    int highscore = 0;
    for (auto& statistic : res.statistics )
    {
        msg += statistic + ", ";
        if (std::stoi(statistic) > highscore)
        {
            highscore = std::stoi(statistic);
        }
    }
    data["userStatistic"] = msg;
    data["high score"] = highscore;
    return response(data, OP_CODES::HIGH_SCORE);
}

Buffer JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse res)
{
    json data;
    data["status"] = res.status;
    return response(data, OP_CODES::CLOSE_ROOM);
}

Buffer JsonResponsePacketSerializer::serializeResponse(StartGameResponse res)
{
    json data;
    data["status"] = res.status;
    return response(data, OP_CODES::START_GAME);
}

Buffer JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse res)
{
    json data;
    data["status"] = res.status;
    data["hasGameBegun"] = res.hasGameBegun;
    std::string msg;
    for (auto& player : res.players)
    {
        msg += player + ", ";
    }
    data["players"] = msg;
    data["timePerQuestion"] = res.timePerQuestion;
    return response(data, OP_CODES::GET_ROOM_STATE);
 
}

Buffer JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse res)
{
    json data;
    data["status"] = res.status;
    return response(data, OP_CODES::LEAVE_ROOM);
}


