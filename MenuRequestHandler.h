
#pragma once
#include "IRequestHandler.h"
#include "StatisticsManager.h"
#include "RoomManager.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "RequestHandlerFactory.h"

class RequestHandlerFactory;

class MenuRequestHandler :IRequestHandler
{
public:
	MenuRequestHandler(StatisticsManager* statisticsManager, RoomManager* roomManager, RequestHandlerFactory* handlerFactory);
	~MenuRequestHandler() { };
	bool isRequestRelevant(RequestInfo) override;
	RequestResult handleRequest(RequestInfo) override;

private:
	LoggedUser* m_user;
	RequestHandlerFactory* m_handlerFactory;
	StatisticsManager* m_statisticsManager;
	RoomManager* m_roomManager;

	RequestResult logout(RequestInfo info);
	RequestResult getRooms(RequestInfo info);
	RequestResult getStatistics(RequestInfo info);
	RequestResult joinRoom(RequestInfo info);
	RequestResult createRoom(RequestInfo info);
	RequestResult getPlayersInRoom(RequestInfo info);
};


