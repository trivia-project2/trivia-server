#pragma once
#include <iostream>
#include "Buffer.h"
#include "json.hpp"
#include "Responses.h"
using json = nlohmann::json;

class JsonResponsePacketSerializer
{
public:
	static Buffer response(json data, OP_CODES code);
	static Buffer serializeResponse(LoginResponse res);
	static Buffer serializeResponse(SignupResponse res);
	static Buffer serializeResponse(ErrorResponse res);
	static Buffer serializeResponse(LogoutResponse res);
	static Buffer serializeResponse(GetRoomsResponse res);
	static Buffer serializeResponse(GetPlayersInRoomResponse res);
	static Buffer serializeResponse(JoinRoomResponse res);
	static Buffer serializeResponse(CreateRoomResponse res);
	static Buffer serializeResponse(HighScoreResponse res);
};